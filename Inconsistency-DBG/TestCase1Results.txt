Parameters:
    fen: r1bqk2r/pppp1Npp/2n2n2/2b1p3/2B1P3/8/PPPP1PPP/RNBQK2R b KQkq - 0 5
    move: c5f2


Run 1:

Access!
eval is :3.28, eval2 is: 1.67
That move was a small mistake. It was a mildly bad move.
['Best move', 3.28, ['d8e7', 'c5f2', 'f6e4'], 1.67, ['e1f2', 'e1f1', 'e1e2']]

Run 2:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Best move', 2.57, ['d8e7', 'c5f2', 'f6e4'], 4.12, ['e1f2', 'e1f1', 'e1e2']]

Run 3:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Small Mistake', 2.57, ['d8e7', 'f6e4', 'c6a5'], 4.12, ['e1f2', 'e1f1', 'e1e2']]


Run 4:

Access!
eval is :3.28, eval2 is: 1.13
['Best move', 3.28, ['d8e7', 'c5f2', 'f6e4'], 1.13, ['e1f2', 'e1f1', 'e1e2']]

Run 5:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Small Mistake', 2.57, ['d8e7', 'f6e4', 'c6a5'], 4.12, ['e1f2', 'e1f1', 'e1e2']]

Run 6:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Small Mistake', 2.57, ['d8e7', 'f6e4', 'c6a5'], 4.12, ['e1f2', 'e1f1', 'e1e2']]

Run 7:

Access!
eval is :2.57, eval2 is: 2.57
['Best move', 2.57, ['c5f2', 'd8e7', 'f6e4'], 2.57, ['e1f2', 'e1f1', 'e1e2']]

Run 8:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Best move', 2.57, ['d8e7', 'c5f2', 'f6e4'], 4.12, ['e1f2', 'e1f1', 'e1e2']]

Run 9:

Access!
eval is :2.57, eval2 is: 4.12
That move was a small mistake. It was a mildly bad move.
['Small Mistake', 2.57, ['d8e7', 'f6e4', 'c6a5'], 4.12, ['e1f2', 'e1f1', 'e1e2']]
